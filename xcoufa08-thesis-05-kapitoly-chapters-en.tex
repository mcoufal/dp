% ----------------------------------------------------------------------------- %
%                                 C H A P T E R                                 %
% ----------------------------------------------------------------------------- %
\chapter{Experiments}
\label{ch:experiments}
\tbr{The actual optimal values of hyper-parameter settings in neural networks are not known and moreover, the training time of the NN might be substantial. Therefore, it is common to test the performance of hyper-optimisation on benchmarks first~\cite{eggensperger_sbhp}. Benchmarks are functions that are designed to simulate the typical difficulties that can occur during hyper-optimisation while searching the hyper-parameters domains~\cite{hansen_benchmarks}. To evaluate the efficiency of the implemented optimizers and their features, experiments were first conducted on benchmarks and then on a NN using MNIST dataset \cite{lecun_mnist}.}

\tbr{This chapter contains description of implemented benchmarks and the results of the conducted experiments.}

% ----------------------------------------------------------------------------- %
% ----------------------------------------------------------------------------- %
\section{Benchmarks for Hyper-parameter Optimisation}
\label{testing_benchmarks_description}
\tbr{Since function parameters can be viewed as a particular hyper-parameters with continuous domains and the resulting function value as a result of the loss function, the benchmark functions are applicable substitute for actual NN. The advantage is that the minimal input vectors of such functions are known and can be even directly specified in the benchmark. Two basic benchmarks, Sphere and Ellipsoidal~\cite{hansen_benchmarks}, were used in the experiments. Both represent unimodal, separable problems and are scalable with dimension. Unimodal means that the function has only one local minimum (maximum) and it's the global minimum (maximum). The separability of \emph{D-dimensional} problem means that it can be separated into $D$ one-dimensional procedures and solved independently.}

Defined benchmarks use the following notation: $\mathbf{x}$ is \emph{D-dimensional} input vector, $\mathbf{x}^{opt}$ is an optimal solution vector and $f_{opt}$ is minimal function value such that $f_{opt} = f(\mathbf{x}^{opt})$.

% ----------------------------------------------------------------------------- %
\subsection{Sphere Function}
\tbr{Sphere function represents easy continuous domain search problem that is unimodal and highly symmetric. Sphere function is given as:}

\begin{equation}
    \begin{split}
        f(\mathbf{x}) &= \|\mathbf{z}\|^2 + f_{opt} \\
        \mathbf{z} &= \mathbf{x} - \mathbf{x}^{opt}
    \end{split}
\end{equation}

\noindent
\tbr{An example of two-dimensional Sphere function is shown in Figure~\ref{fig:sphere_function}.}

\begin{figure}[h]
  \begin{subfigure}[t]{.48\textwidth}
    \centering
    \includegraphics[width=1.05\linewidth,trim={2.5cm 0cm 0.2cm 2.5cm},clip]{obrazky-figures/benchmarks/sphere_benchmark_3D.pdf}
    \caption{3D plot}
    \label{fig:sphere_benchmark_3D}
  \end{subfigure}
  \hfill
  \begin{subfigure}[t]{.48\textwidth}
    \centering
    \includegraphics[width=.9\linewidth]{obrazky-figures/benchmarks/sphere_benchmark_contour.pdf}
    \caption{contour plot}
    \label{fig:sphere_benchmark_contour}
  \end{subfigure}
  \caption{Example of a two-dimensional Sphere benchmark. Optimal function value $f(\mathbf{x}^{opt})$ in optimum $\mathbf{x}^{opt} = (2.5, 2.5)$ is shown by a red arrow/cross.}
  \label{fig:sphere_function}
\end{figure}

% ----------------------------------------------------------------------------- %
\subsection{Ellipsoidal Function}

\tbr{Ellipsoidal function represents unimodal, ill-conditioned continuous search problem with smooth local irregularities. Ill-conditioned function is a function such that a small change in the input vector of the function may lead to a large change in the resulting function value. Ellipsoidal function is defined as~\cite{hansen_benchmarks}:}

\begin{equation}
    \begin{split}
        f(x) &= \sum_{i = 1}^{D} 10^{6 \frac{i - 1}{D - 1}} z_i^2 + f_{opt} \\
        \mathbf{z} &= T_{osz}(\mathbf{x} - \mathbf{x}^{opt})
    \end{split}
\end{equation}

\noindent
\tbr{where $T_{osz}: \mathbb{R}^D \rightarrow \mathbb{R}^D$ is mapped element-wise for each element of the input vector and is defined as follows:}

\begin{equation}
    \begin{split}
        x &\mapsto \text{sign}(x) \text{exp}(\hat{x} + 0.049 (\text{sin}(c_1 \hat{x}) + \text{sin}(c_2 \hat{x}))) \\
        \hat{x} &=
        \begin{cases}
            \text{log}(\abs{x}) &\text{ if } x \neq 0 \\
            0 &\text{ otherwise}
        \end{cases}\quad
        \text{sign}(x) =
        \begin{cases}
            -1 &\text{ if } x < 0 \\
            0 &\text{ if } x = 0 \\
            1 &\text{ otherwise}
        \end{cases} \\
        c_1 &=
        \begin{cases}
            10 &\text{ if } x > 0 \\
            5.5 &\text{ otherwise}
        \end{cases}\qquad\qquad\quad\:
        c_2 =
        \begin{cases}
            7.9 &\text{ if } x > 0 \\
            3.1 &\text{ otherwise}
        \end{cases}
    \end{split}
\end{equation}

\noindent
\tbr{Example of two-dimensional Ellipsoidal function is shown in Figure~\ref{fig:ellipsoidal_function}.}

\begin{figure}[h]
  \begin{subfigure}[t]{.48\textwidth}
    \centering
    \includegraphics[width=\linewidth,trim={2.6cm 0cm 0.4cm 2.4cm},clip]{obrazky-figures/benchmarks/ellipsoidal_benchmark_3D.pdf}
    \caption{3D plot}
    \label{fig:ellipsoidal_benchmark_3D}
  \end{subfigure}
  \hfill
  \begin{subfigure}[t]{.48\textwidth}
    \centering
    \includegraphics[width=.9\linewidth]{obrazky-figures/benchmarks/ellipsoidal_benchmark_contour.pdf}
    \caption{contour plot}
    \label{fig:ellipsoidal_benchmark_contour}
  \end{subfigure}
  \caption{Example of a two-dimensional Ellipsoidal benchmark. Optimal function value $f(\mathbf{x}^{opt})$ in optimum $\mathbf{x}^{opt} = (2.5, 2.5)$ is shown by a red arrow/cross. As can be seen, the benchmark is ill-conditional only in dimension of $x_1$, while $x_2$ has only a small influence on the resulting function value.}
  \label{fig:ellipsoidal_function}
\end{figure}

% ----------------------------------------------------------------------------- %
% ----------------------------------------------------------------------------- %
\section{Testing the Toolkit on Benchmarks}
\label{testing_benchmarks_experiments}

\tbr{Each of the listed experiments on the before mentioned benchmark functions has been averaged from over 100 runs with different function optimum. The location of the optimum was selected randomly with uniform distribution from a subset of available search space, so the optimum lies in different sector of the search space in each run. These measures should provide reasonable assessment in performance of all tested features of GPOP.}

\tbr{Each experiment was run on a benchmark with different dimensionality, where each dimension was optimised on interval $[0, 5]$. Based on the dimensionality of the benchmark, different number of samples and optimisation steps were used to comply with the size of the search space. The number of samples and the number of optimisation steps used for different $D$-dimensional benchmarks are shown in Table~\ref{tab:rounds-samples}. All experiments used Matérn ARD 5/2 kernel, random DSS and EI AF, any additional settings or differences are described in particular experiments.}

\begin{table}[hbt]
\centering
\caption{Number of optimisation steps and number of testing samples used in $D$-dimensional benchmark experiments.}
\label{tab:rounds-samples}
\begin{tabular}{@{}llllll@{}}
\toprule
Configuration & 1D     & 2D     & 3D     & 4D     & 5D \\ \midrule
\#steps       & $10$   & $15$   & $20$   & $25$   & $30$ \\
\#samples     & $25$   & $20^2$ & $15^3$ & $10^4$ & $5^5$ \\
\bottomrule
\end{tabular}
\end{table}

\tbr{Experiments are divided into five sections, where first section focuses on comparison of implemented optimisers, next sections are focused on selected kernel, DSS and AF, while the last part focuses on automatic tuning of the GP parameters.}

% ----------------------------------------------------------------------------- %
\subsection{Optimiser Accuracy Comparison}

\tbr{The first set of experiments devotes attention to comparison between GP, grid and random hyper-optimisation. All of the optimisers were tested on up to 5-dimensional benchmarks. The accuracy of each optimiser is highly influenced by the size of search space and properties of the benchmark function. In this set of experiments, GP optimiser with RBF kernel, random DSS and Expected Improvement AF was used.}

\tbr{Concerning smaller search space and sufficient amount of optimisation steps, Grid optimiser may provide better results than Random optimiser. But as the number of optimisation steps reduces or search space increases, Grid optimiser starts to provide worse results than GP optimiser or Random optimiser (see Figure~\ref{fig:opt_compare_1D_2D}).}

\begin{figure}[h]
  \begin{subfigure}{0.49\linewidth}
    \centering
    \includegraphics[width=\linewidth,trim={3cm 0cm 4cm 2cm},clip]{obrazky-figures/optimisers/opt_compare_sphere_1D_detail_5-10.pdf}
    \caption{1D Sphere}
    \label{fig:opt_compare_sphere_1D_detail_5-10}
  \end{subfigure}
  \hfill
  \begin{subfigure}{0.49\linewidth}
    \centering
    \includegraphics[width=\linewidth,trim={3cm 0cm 4cm 2cm},clip]{obrazky-figures/optimisers/opt_compare_sphere_2D_detail_5-15.pdf}
    \caption{2D Sphere}
    \label{fig:opt_compare_sphere_2D_detail_5-15}
  \end{subfigure}
  \caption{Comparison of GP, Random and Grid optimisers on 1D and 2D Sphere benchmarks showing the cumulative minimum over optimisation steps.}
  \label{fig:opt_compare_1D_2D}
\end{figure}

\tbr{As can be seen in Figure~\ref{fig:opt_compare_3D}, there is a mild difference between optimisation of Sphere and Ellipsoidal benchmark considering GP and random optimisation. Sphere benchmark is highly symmetrical and contains larger space with values closer to optimum. Therefore, Random optimiser tends to provide better results in the first few steps of optimisation (first 8 steps in Figure~\ref{fig:opt_compare_sphere_3D_detail_3-20}) before GP optimiser creates sufficient model. Ellipsoidal benchmark is less symmetrical and ill-conditioned and therefore GP optimiser finds better solution faster, as can be seen in Figure~\ref{fig:opt_compare_ellipsoidal_3D_detail_3-20}.}

\begin{figure}[h]
  \begin{subfigure}{0.49\linewidth}
    \centering
    \includegraphics[width=\linewidth,trim={2cm 0cm 4cm 2cm},clip]{obrazky-figures/optimisers/opt_compare_sphere_3D_detail_3-20.pdf}
    \caption{3D Sphere}
    \label{fig:opt_compare_sphere_3D_detail_3-20}
  \end{subfigure}
  \hfill
  \begin{subfigure}{0.49\linewidth}
    \centering
    \includegraphics[width=\linewidth,trim={2cm 0cm 4cm 2cm},clip]{obrazky-figures/optimisers/opt_compare_ellipsoidal_3D_detail_3-20.pdf}
    \caption{3D Ellipsoidal}
    \label{fig:opt_compare_ellipsoidal_3D_detail_3-20}
  \end{subfigure}
  \caption{Comparison of GP, Random and Grid optimisers on 3D benchmarks showing the cumulative minimum over optimisation steps.}
  \label{fig:opt_compare_3D}
\end{figure}

\tbr{The number of steps needed for GP optimiser to beat average values of Random optimiser is influenced by the shape of the problem and by the size of the search space. When searching in larger or less symmetric space of a benchmark, the chances of random search to select better values are lesser. That means GP optimisation can achieve better results in just a few steps even when optimising problems with more dimensions, as can be seen in Figure~\ref{fig:opt_compare_5D}.}

\begin{figure}[h]
  \begin{subfigure}{0.49\linewidth}
    \centering
    \includegraphics[width=\linewidth,trim={1.5cm 0cm 4cm 2cm},clip]{obrazky-figures/optimisers/opt_compare_sphere_5D_detail_3-30.pdf}
    \caption{5D Sphere}
    \label{fig:opt_compare_sphere_5D_detail_3-30}
  \end{subfigure}
  \hfill
  \begin{subfigure}{0.49\linewidth}
    \centering
    \includegraphics[width=\linewidth,trim={1.5cm 0cm 4cm 2cm},clip]{obrazky-figures/optimisers/opt_compare_ellipsoidal_5D_detail_3-30.pdf}
    \caption{5D Ellipsoidal}
    \label{fig:opt_compare_ellipsoidal_5D_detail_3-30}
  \end{subfigure}
  \caption{Comparison of GP and Random optimiser on 5D benchmarks showing the cumulative minimum over optimisation steps. Random optimiser performs very-well on highly symmetrical Sphere benchmark and manages to provide better results for about 20 optimisation steps, while GP optimiser provides better optimisation results on Ellipsoidal benchmark.}
  \label{fig:opt_compare_5D}
\end{figure}

% ----------------------------------------------------------------------------- %
\subsection{Kernel Experiments}

\tbr{Kernel experiments were focused on performance of kernels themselves, without any differences between the parameters of the kernels. All three tested kernels (RBF, Laplacian and Matérn 5/2) used the same parameters setting in each conducted experiment, therefore Matérn kernel was used without ARD. Experiments were run on both before mentioned benchmarks in up to $5$-dimensions.}

\tbr{As shown in Figure~\ref{fig:kernel_experiments}, kernel selection has a significant influence on the optimisation. The difference between the tested kernels on one and two-dimensional benchmarks is inconspicuous, but experiments on larger search space and in more dimensions show that Laplacian kernel suites these benchmarks better than the other two kernels. The performance of RBF and Matérn kernel is quite similar, but in most of the experiments RBF kernel performed slightly better. This is expected, since the behaviour of the kernels is quite similar and while Matérn is more suitable to model more substantial local changes, both used benchmarks are rather smooth.}

\begin{figure}[h]
  \begin{subfigure}{0.49\linewidth}
    \centering
    \includegraphics[width=\linewidth,trim={1.5cm 0cm 4cm 2cm},clip]{obrazky-figures/kernel/kernel_comparison_sphere_4D.pdf}
    \caption{4D Sphere}
    \label{fig:kernel_comparison_sphere_4D}
  \end{subfigure}
  \hfill
  \begin{subfigure}{0.49\linewidth}
    \centering
    \includegraphics[width=\linewidth,trim={1.5cm 0cm 4cm 2cm},clip]{obrazky-figures/kernel/kernel_comparison_ellipsoidal_4D.pdf}
    \caption{4D Ellipsoidal}
    \label{fig:kernel_comparison_ellipsoidal_4D}
  \end{subfigure}
  \caption{Comparison of implemented kernels on 4D Sphere and Ellipsoidal benchmarks showing the cumulative minimum over optimisation steps. Kernel selection is highly dependent on the optimised problem and might reduce the number of optimisation steps quite significantly.}
  \label{fig:kernel_experiments}
\end{figure}

% ----------------------------------------------------------------------------- %
\subsection{DSS Experiments}

\tbr{Next set of experiments was focused on implemented domain-space search strategies. The main objective of this set of experiments was to determine how different DSS strategies influence the result of hyper-optimisation and how are particular strategies influenced by used number of testing samples. Due to the high number of experiments needed to evaluate the latter, only benchmarks with up to $3$-dimensions were tested, while the first set of experiments was run on up to $5$-dimensional benchmarks.}

\tbr{When considering the influence of the number of samples on both DSS strategies, the optimal number of samples depends on the optimised benchmark, the number of optimisation steps, the number of dimensions and the size of the search space. As shown in Figure~\ref{fig:dss_samples_avg}, the average performance of both grid and random DSS seem to share the same behaviour on each benchmark, except for smaller number of samples. All conducted DSS experiments in up to 5D show that starting from approximately $10^D$ samples (where $D$ is the number of dimensions), the behaviour of both averages is very similar.}

\begin{figure}[h]
  \begin{subfigure}[t]{0.49\linewidth}
    \centering
    \includegraphics[width=\linewidth,trim={1cm 0cm 4cm 2cm},clip]{obrazky-figures/dss/samples_sphere_3D_avg.pdf}
    \caption{3D Sphere}
    \label{fig:samples_sphere_3D_avg}
  \end{subfigure}
  \hfill
  \begin{subfigure}[t]{0.49\linewidth}
    \centering
    \includegraphics[width=\linewidth,trim={1cm 0cm 4cm 2cm},clip]{obrazky-figures/dss/samples_ellipsoidal_3D_avg.pdf}
    \caption{3D Ellipsoidal}
    \label{fig:samples_ellipsoidal_3D_avg}
  \end{subfigure}
  \caption{Influence of the number of samples on average error of all the optimisation steps. The behaviour of both strategies is quite similar, except for very small number of samples. In comparison with grid DSS, random DSS achieves surprisingly good results even for a really small number of testing samples. However, this pattern isn't that striking in 1D experiments.}
  \label{fig:dss_samples_avg}
\end{figure}

\tbr{The average values serve as a good guideline for the comparison of a behaviour of both strategies, but they do not show the actual best achieved results. Figure~\ref{fig:dss_samples_steps} shows the best achieved results for selected optimisation steps in dependency on the number of samples. Because the results for each tested number of steps depends on so many factors, it's hard to select suitable number of testing samples. But it's obvious that grid DSS achieves poor results with small number of optimisation steps, while random DSS achieves more consistent results for every tested number of samples and moreover, it performs quite well even with a small number of testing samples.}

\begin{figure}[h]
  \begin{subfigure}[t]{0.49\linewidth}
    \centering
    \includegraphics[width=\linewidth,trim={3cm 0cm 4cm 2cm},clip]{obrazky-figures/dss/samples_sphere_3D_grid.pdf}
    \caption{grid DSS on 3D Sphere}
    \label{fig:samples_sphere_3D_grid}
  \end{subfigure}
  \hfill
  \begin{subfigure}[t]{0.49\linewidth}
    \centering
    \includegraphics[width=\linewidth,trim={3cm 0cm 4cm 2cm},clip]{obrazky-figures/dss/samples_sphere_3D_random.pdf}
    \caption{random DSS on 3D Sphere}
    \label{fig:samples_sphere_3D_random}
  \end{subfigure}
  \begin{subfigure}[t]{0.49\linewidth}
    \centering
    \includegraphics[width=\linewidth,trim={3cm 0cm 4cm 2cm},clip]{obrazky-figures/dss/samples_ellipsoidal_3D_grid.pdf}
    \caption{grid DSS on 3D Ellipsoidal}
    \label{fig:samples_ellipsoidal_3D_grid}
  \end{subfigure}
  \hfill
  \begin{subfigure}[t]{0.49\linewidth}
    \centering
    \includegraphics[width=\linewidth,trim={3cm 0cm 4cm 2cm},clip]{obrazky-figures/dss/samples_ellipsoidal_3D_random.pdf}
    \caption{random DSS on 3D Ellipsoidal}
    \label{fig:samples_ellipsoidal_3D_random}
  \end{subfigure}
  \caption{Influence of the number of samples on error in some of the optimisation steps. As can be seen in figures above, the best selected number of testing samples depends on many factors, such as selected DSS, number of optimisation steps and optimised benchmark.}
  \label{fig:dss_samples_steps}
\end{figure}

\tbr{Both random DSS and grid DSS behave analogously when changing the number of samples (except for very small values) and therefore experiments focusing on the influence of DSS strategies on minimizing the benchmark functions were run with the same amount of samples for both strategies. As results on Figure~\ref{fig:dss_basic} suggest, hyper-optimisation with random DSS achieved better results then grid DSS in all conducted experiments. Furthermore, the difference between both approaches is more notable when solving benchmarks with more dimensions and experiments conducted on Ellipsoidal benchmark showed more substantial differences between tested DSS strategies.}

\begin{figure}[h]
  \begin{subfigure}[t]{0.49\linewidth}
    \centering
    \includegraphics[width=\linewidth,trim={2cm 0cm 4cm 2cm},clip]{obrazky-figures/dss/basic_sphere_1D_all.pdf}
    \caption{1D Sphere benchmark}
    \label{fig:basic_sphere_1D_all}
  \end{subfigure}
  \hfill
  \begin{subfigure}[t]{0.49\linewidth}
    \centering
    \includegraphics[width=\linewidth,trim={2cm 0cm 4cm 2cm},clip]{obrazky-figures/dss/basic_ellipsoidal_3D_all.pdf}
    \caption{3D Ellipsoidal}
    \label{fig:basic_ellipsoidal_3D_all}
  \end{subfigure}
  \caption{Comparison of grid and random DSS.}
  \label{fig:dss_basic}
\end{figure}

% ----------------------------------------------------------------------------- %
\subsection{Comparison of Acquisition Functions}

\tbr{Next set of experiments compared three acquisition functions: \texttt{MinimalMean}, \texttt{LowerConfidence} and \texttt{ExpectedImprovement}. Experiments on one-dimensional benchmarks showed the best results achieved \texttt{LowerConfidence} AF, while the rest of the experiments (up to 5D for both benchmarks) showed better convergence to optimal value while using \texttt{ExpectedImprovement} AF. As shown in Figure~\ref{fig:af_basic}, the difference between used AF was most perceptible in experiments with Ellipsoidal benchmark, while results on Sphere benchmark show only minimal difference between used AFs.}

\begin{figure}[h]
  \begin{subfigure}[t]{0.49\linewidth}
    \centering
    \includegraphics[width=\linewidth,trim={1cm 0cm 4cm 2cm},clip]{obrazky-figures/af/basic_sphere_4D.pdf}
    \caption{4D Sphere}
    \label{fig:basic_sphere_4D}
  \end{subfigure}
  \hfill
  \begin{subfigure}[t]{0.49\linewidth}
    \centering
    \includegraphics[width=\linewidth,trim={1cm 0cm 4cm 2cm},clip]{obrazky-figures/af/basic_ellipsoidal_4D.pdf}
    \caption{4D Ellipsoidal}
    \label{fig:basic_ellipsoidal_4D}
  \end{subfigure}
  \caption{Influence of selected AF on the result of the optimisation. All AFs in experiments on Sphere benchmarks performed almost identically, except \texttt{ExpectedImprovement} AF was able to converge closer to the benchmark optimal function value, if given enough optimisation steps. This difference was more significant on Ellipsoidal benchmarks, where except first few optimisation steps, \texttt{ExpectedImprovement} AF achieved distinctly better results that the other two AFs (note that the results for \texttt{MinimalMean} and \texttt{LowerConfidence} shown in Figure b) coincide).}
  \label{fig:af_basic}
\end{figure}

% ----------------------------------------------------------------------------- %
\subsection{Estimation of GP Parameters}

\tbr{The last part of experiments on benchmarks was focused on automatic tuning of GP parameters. Three options were tested: GP optimisation without automatic tuning, with automatic tuning of kernel parameters and with automatic tuning of all parameters (kernel and uncertainty).}

\tbr{The best results on most of the benchmarks were achieved with automatic tuning of kernel parameters, as shown in Figure~\ref{fig:autotune}. Mentioned problem with tuning the uncertainty is probably caused because of the automatic tuning algorithm, which is trying to improve the log likelihood by increasing the uncertainty. That leads to the possibility of also increasing characteristic length scale, so these two parameters are increased until the model no longer fits the problem.}

\begin{figure}[h]
  \begin{subfigure}[t]{0.49\linewidth}
    \centering
    \includegraphics[width=\linewidth,trim={1.5cm 0cm 4cm 2cm},clip]{obrazky-figures/mle/sphere_1D.pdf}
    \caption{Sphere 1D}
    \label{fig:sphere_1D}
  \end{subfigure}
  \hfill
  \begin{subfigure}[t]{0.49\linewidth}
    \centering
    \includegraphics[width=\linewidth,trim={1.5cm 0cm 4cm 2cm},clip]{obrazky-figures/mle/sphere_3D.pdf}
    \caption{Sphere 3D}
    \label{fig:sphere_3D}
  \end{subfigure}
  \begin{subfigure}[t]{0.49\linewidth}
    \centering
    \includegraphics[width=\linewidth,trim={1.5cm 0cm 4cm 2cm},clip]{obrazky-figures/mle/sphere_5D.pdf}
    \caption{Sphere 5D}
    \label{fig:sphere_5D}
  \end{subfigure}
  \hfill
  \begin{subfigure}[t]{0.49\linewidth}
    \centering
    \includegraphics[width=\linewidth,trim={1.5cm 0cm 4cm 2cm},clip]{obrazky-figures/mle/ellipsoidal_5D.pdf}
    \caption{Ellipsoidal 5D}
    \label{fig:ellipsoidal_5D}
  \end{subfigure}
  \caption{Influence of automatic tuning of GP parameters on a benchmark optimisation. One-dimensional benchmarks show only minor differences, but multidimensional benchmarks show that the automatic tuning of kernel parameters performs the best, while tuning of uncertainty leads in most cases to short improvement followed by significant deterioration of the optimisation. Though in one case the use of GP optimiser without automatic tuning achieved better results that GP optimiser with automatic tuning of kernel parameters, most experiments show that the latter yields significant improvement of the optimisation.}
  \label{fig:autotune}
\end{figure}

% ----------------------------------------------------------------------------- %
% ----------------------------------------------------------------------------- %
\section{Neural Networks Experiments}
\label{sec:gpop_nn_experiments}

\tbr{NN experiments were run on MNIST dataset, specifically on image classification example NN\footnote{https://github.com/pytorch/examples/tree/master/mnist}. NN in the example uses Stochastic Gradient Descent algorithm and trains the network for 10 epochs. All experiments compare three different optimisers: Grid optimiser, Random optimiser and GP optimiser. GP optimiser was run in two configurations, once with fixed parameters and once with automatic tuning of kernel parameters.}

\tbr{All optimised hyper-parameters and used dimension bounds are presented in Table~\ref{tab:hp_bounds}. GP optimiser used random DSS, EI AF, uncertainty of $1\mathrm{e}{-3}$ and Matérn ARD 5/2 kernel with signal standard deviation $\sigma_{f} = 1.0$ and two distinct settings of characteristic length scale $\pmb{\sigma}_l$, as defined in Table \ref{tab:sigma_settings}. Used number of optimisation steps and number of samples is the same as in benchmarks experiments and is described in Table~\ref{tab:rounds-samples}.}

\begin{table}[hbt]
\centering
\caption{Optimised hyper-parameters and their bounds used in experiments.}
\label{tab:hp_bounds}
\begin{tabular}{@{}lll@{}}
\toprule
Hyper-parameter                    & Type  & Dimension bounds \\ \midrule
learning rate                      & float & $\interval{0}{1}$ \\
number of neurons in hidden layers & int   & $\interval{32}{512}$ \\
batch size                         & int   & $\interval{1}{1024}$ \\
\bottomrule
\end{tabular}
\end{table}

\begin{table}[hbt]
\centering
\caption{Characteristic length scale settings for optimised hyper-parameters used in experiments.}
\label{tab:sigma_settings}
\begin{tabular}{@{}lll@{}}
\toprule
\multirow{2}{*}{Hyper-parameter}   & \multicolumn{2}{c}{$\pmb{\sigma}_l$} \\
\cmidrule{2-3}
                                   & setting 1   & setting 2 \\
\midrule
learning rate                      & 0.1 & 0.2 \\
number of hidden neurons & 48  & 96 \\
batch size                         & 102 & 204 \\
\bottomrule
\end{tabular}
\end{table}

\wip{All results show cumulative minimum of validation loss in different optimisation steps. All loss values are averaged from only $5$ distinct optimisation runs, due to a longer training time of the NN. This causes noticeable dispersion of the resulting loss values, but it should provide reasonable estimate to roughly compare the optimisers. Note that every experiment was run with two different settings of kernel parameter characteristic length scale $\pmb{\sigma}_l$.}

\wip{The aim of the first set of experiments was optimising a single hyper-parameter. As can be seen in Figure \ref{fig:1D_mnist}, Grid optimiser managed to find the best hyper-parameter setting of learning rate and number of hidden neurons. Grid optimiser searches the domain progressively, so the first and the last few optimisation steps search the border of a domain. That is the reason why the cumulative minimum of Grid optimiser usually changes rapidly in the beginning and maintains the value at the end of the optimisation. Random optimiser achieved the best result in optimisation of the batch size. But unlike Grid optimiser, its result is not influenced by the location of the optimum and the therefore the change in cumulative minimum is more gradual. GP optimiser needs a few steps before it creates sufficient model and then starts to improve it's optimisation, but was not able to provide better results than the other optimisers.}

\begin{figure}[h]
  \begin{subfigure}[t]{0.49\linewidth}
    \centering
    \includegraphics[width=\linewidth,trim={2.5cm 0cm 4cm 2cm},clip]{obrazky-figures/mnist/1D_lr_params2.pdf}
    \caption{learning rate}
    \label{fig:1D_lr_params2}
  \end{subfigure}
  \hfill
  \begin{subfigure}[t]{0.49\linewidth}
    \centering
    \includegraphics[width=\linewidth,trim={2.5cm 0cm 4cm 2cm},clip]{obrazky-figures/mnist/1D_nh_params2.pdf}
    \caption{number of hidden neurons}
    \label{fig:1D_nh_params2}
  \end{subfigure}
  \centering
  \begin{subfigure}[t]{0.49\linewidth}
    \centering
    \includegraphics[width=\linewidth,trim={2.5cm 0cm 4cm 2cm},clip]{obrazky-figures/mnist/1D_bs_params2.pdf}
    \caption{batch size}
    \label{fig:1D_bs_params2}
  \end{subfigure}
  \caption{Comparison of Grid, Random and GP optimisers on MNIST NN, optimising one hyper-parameter.}
  \label{fig:1D_mnist}
\end{figure}

\wip{The second set of experiments focused on optimisation of two hyper-parameters. As can be seen in Figure \ref{fig:2D_mnist}, Random optimiser achieved better result in the first few steps of optimisation, while Grid optimiser managed to find better values in two out of three experiments. GP optimiser performed similarly as in optimisation of one hyper-parameter, but there are more noticeable differences between GP optimiser with and without automatic parameter tuning. These differences are probably more noticeable because of higher number of optimisation steps with automatic parameter tuning.}

\begin{figure}[h]
  \begin{subfigure}[t]{0.49\linewidth}
    \centering
    \includegraphics[width=\linewidth,trim={1.5cm 0cm 4cm 2cm},clip]{obrazky-figures/mnist/2D_lr_nh_params2.pdf}
    \caption{learning rate, number of hidden neurons}
    \label{fig:2D_lr_nh_params2}
  \end{subfigure}
  \hfill
  \begin{subfigure}[t]{0.49\linewidth}
    \centering
    \includegraphics[width=\linewidth,trim={1.5cm 0cm 4cm 2cm},clip]{obrazky-figures/mnist/2D_lr_bs_params2.pdf}
    \caption{learning rate, batch size}
    \label{fig:2D_lr_bs_params2}
  \end{subfigure}
  \centering
  \begin{subfigure}[t]{0.49\linewidth}
    \centering
    \includegraphics[width=\linewidth,trim={1.5cm 0cm 4cm 2cm},clip]{obrazky-figures/mnist/2D_nh_bs_params2.pdf}
    \caption{number of hidden neurons, batch size}
    \label{fig:2D_nh_bs_params2}
  \end{subfigure}
  \caption{Comparison of Grid, Random and GP optimisation of two hyper-parameters of MNIST NN.}
  \label{fig:2D_mnist}
\end{figure}

\wip{The last set of experiments was focused on optimisation of all three hyper-parameters. As can be seen in Figure \ref{fig:3D_lr_nh_bs}, Random optimiser provided the best results throughout all the optimisation steps, while GP optimiser provided better results than Grid optimiser the first $13$ steps. But this is influenced by the number of optimisation steps in total and Grid optimiser might possibly find better hyper-parameter settings even when using fewer optimisation steps. Also, note that GP optimiser in Figure \ref{fig:3D_lr_nh_bs_params2} managed to improve its results to the same loss values as in Figure \ref{fig:3D_lr_nh_bs_params1} by using automatic parameter tuning.}

\begin{figure}[h]
    \begin{subfigure}[t]{0.49\linewidth}
      \centering
      \includegraphics[width=\linewidth,trim={2.5cm 0cm 4cm 2cm},clip]{obrazky-figures/mnist/3D_lr_nh_bs_params1.pdf}
      \caption{$\pmb{\sigma}_l = (0.1,48,102)$}
      \label{fig:3D_lr_nh_bs_params1}
    \end{subfigure}
    \hfill
    \begin{subfigure}[t]{0.49\linewidth}
      \centering
      \includegraphics[width=\linewidth,trim={2.5cm 0cm 4cm 2cm},clip]{obrazky-figures/mnist/3D_lr_nh_bs_params2.pdf}
      \caption{$\pmb{\sigma}_l = (0.2,96,24)$}
      \label{fig:3D_lr_nh_bs_params2}
    \end{subfigure}
    \caption{Comparison of Grid, Random and GP optimisers on MNIST NN, optimising three hyper-parameters: learning rate, number of hidden neurons and batch size. The results for two different characteristic length scale settings are shown. Note that both runs used the same Grid and Random optimiser, the differences in Random optimiser are caused only by different seed.}
    \label{fig:3D_lr_nh_bs}
\end{figure}

\wip{The worse results of GP optimiser might be caused by several issues: GP parameter settings, inconvenient mean or the nature of the loss space. To fix parameters of the GP model, more knowledge about the behaviour of the optimised hyper-parameters is needed. Problems with mean could arise when the loss values are too close to mean value and GP optimiser might get stuck in one place. This issue could be resolved by selection of a different AF or a loss function. Loss space could cause problems when the domains of the optimised hyper-parameters are quite large and the characteristic length scale parameter is set to higher values. That could lead to inability to model the subtle differences in the loss value.}
