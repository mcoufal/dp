% ----------------------------------------------------------------------------- %
%                                 C H A P T E R                                 %
% ----------------------------------------------------------------------------- %
\chapter{Neural Networks and their Hyper-parameters}
\label{nn_description}

Artificial neural networks (NN) are computational models loosely inspired by biological neural networks. They consist of subsequent layers, where each layer is composed of individual artificial neurons. The actual architecture of each network and its learning process can vary since neural networks is a broad term involving a lot of different neural network types and learning algorithms that can be applied to various problems. But the basic idea behind all neural networks is the same: based on some training data or feedback from the environment, the learning algorithm updates the neural network's parameters in order to solve the given problem.

% \cite{brownlee_mlm}
Parameters in NN are coefficients of the model itself and can be estimated or learned from data. Concretely, parameters are usually input weights and biases of the individual neurons. Hyper-parameters, on the other hand, influence the structure or learning process of the NN and need to be explicitly set before training of the network.

% ----------------------------------------------------------------------------- %
% ----------------------------------------------------------------------------- %
\section{Structure of Neural Networks}
The basic element of the NN is an artificial neuron, also called a unit or a node. It consists of a number of inputs $x_1...x_n$, usually represented by a vector $\vec{x} = (x_1, ..., x_n)^T$, input weights $\vec{w} = (w_1, ..., w_n)^T$, bias $\Theta$, activation function $f(\cdot)$, and output $y$. The output of the artificial neuron is determined as follows: all input values are multiplied by corresponding weights, and together with bias form the input of the activation function. The activation function takes this input and computes the output value $y = f(\mathbf{x}^T \mathbf{w} + \Theta)$, as shown in Figure~\ref{fig:ann_neuron}. For common activation functions, the power of a single neuron is limited to solving linearly separable problems.

\begin{figure}[h]
    \centering
    \includegraphics[width=.7\linewidth]{obrazky-figures/nn/artificial_neuron.pdf}
    \caption{Basic structure of artificial neuron.}
    \label{fig:ann_neuron}
\end{figure}

More complex problems can be solved by creating layers of neurons, where each layer's output (except from last layer) is the input of the next subsequent layer. The NN consists of the input layer, number of hidden layers, and the output layer as shown in Figure~\ref{fig:ann_architecure}. The neurons in the input layer correspond to network's input, while the output layer represents network's output. The NN has $n$ hidden layers of neurons, where each layer can have different number of neurons. Connections between two layers can be between every two nodes (fully connected layer) or just between some subset of nodes (convolutional layer).

\begin{figure}[h]
    \centering
    \includegraphics[width=.9\linewidth]{obrazky-figures/nn/neural_network.pdf}
    \caption{Basic structure of artificial neural network with $i$ inputs, $n$ hidden convolutional layers $\mathbf{h_1},..., \mathbf{h_n}$ (each consisting of $j$ neurons) and output layer with $k$ outputs.}
    \label{fig:ann_architecure}
\end{figure}

The activation function, the number of hidden layers and the number of neurons in hidden layers are network's most common structural hyper-parameters. Features of these hyper-parameters and their influence on the NN are described in detail in Section~\ref{ssec:structural_hyper-parameters}.

% ----------------------------------------------------------------------------- %
% ----------------------------------------------------------------------------- %
\section{Learning in Neural Networks}

Learning in neural networks is a process of updating the parameters in order to achieve better accuracy. To evaluate this accuracy, network uses an error function and labelled training data. Labelled means each input vector $\mathbf{x}$ has a corresponding target vector $\mathbf{t}$, which denotes desired network's output. Then, a learning algorithm uses the error function $E(\cdot)$ to calculate the error and updates the parameters $\mathbf{w}$ of the NN. The exact principle of updating the parameters differs in each algorithm, but in order to introduce common hyper-parameters further described in Section~\ref{ssec:learning_hyper-parameters}, below is described the principle of training the NN using Gradient Descent algorithm \cite{ruder_ogdoa}.

Gradient Descent in an iterative algorithm that finds a local minimum by updating the weights by a small steps in the opposite direction of the error function's gradient $\nabla E$:

\begin{equation}
    \mathbf{w}^{(\tau + 1)} = \mathbf{w}^{(\tau)} - \eta \nabla E(\mathbf{w}^{(\tau)}),
\end{equation}

\noindent
where $\tau$ is the current step and $\eta$ is the learning rate. The learning rate is a hyper-parameter that controls how much are the weights changed in each step of the algorithm and its effect is further discussed in Section \ref{ssec:learning_hyper-parameters}.

The gradient $\nabla E$ is a vector that points in a direction of fastest increase of the function. By updating the weights in the opposite direction of $\nabla E$, Gradient Descent algorithm approximates to a local minimum as shown in Figure~\ref{fig:gd_local_minima}. To find better optimum, more initial settings of the weight $\mathbf{w}$ can be used, but there is no guarantee that global optimum will be found. Another way of getting out of the local optimum is using momentum hyper-parameter, which is described in the next section. Although, recent studies show that the local minima found by Gradient Descent algorithm in larger, multi-dimensional loss space of the NN are not such a problem, since their quality is comparable to the global minimum~\cite{choromanska_lsmn}.

\begin{figure}[h]
    \centering
    \includegraphics[width=\linewidth]{obrazky-figures/nn/gd_local_minima.pdf}
    \caption{An example of weight optimisation using Gradient Descent algorithm. Figure shows the influence of a weight parameter $w$ on the error function $E(\cdot)$, where the points $w_1$, $w_3$ and $w_5$ are different initial settings of weight $w$, resulting in different progress of Gradient Descent algorithm. The red arrows represent a change of the error when the current weight $w^{\tau}$ is updated to the value $w^{\tau+1}$. The weight update is in the opposite direction to a gradient of the error function $E(\cdot)$ in concrete settings of $w^{\tau}$.}
    \label{fig:gd_local_minima}
\end{figure}

The gradient approximately equals to a derivative of the error function $E(\cdot)$ with respect to the weights $\mathbf{w}$ multiplied with how much are the weights changed~\cite{bushaev_hdwtnn}:

\begin{equation}
    \nabla E \approx \frac{\partial E}{\partial \mathbf{w}} \nabla \mathbf{w}
\end{equation}

Although the gradient can be calculated directly with respect to each weight individually, NNs usually have huge amount of parameters and therefore it's not very usable in practice. Therefore, Backpropagation algorithm is commonly used. Backpropagation uses a local message passing scheme in which the information is sent alternately forwards and backwards through the network to efficiently evaluate gradient one layer at the time using the chain rule~\cite{lecun_tfbp}.

The error function $E(\cdot)$ denotes a measure that evaluates the difference between network's output and target vector and its selection depends solely on the features of the solved problem. Resulting output is called training or validation loss, depending on whether the input of the error function was training or validation data. One of the most common forms of the error function that is used in regression is sum of squares error function:

\begin{equation}
    \label{error_function}
    E(\mathbf{w}) = \frac{1}{2} \sum_{n=1}^{N} \left\lVert \mathbf{y}(\mathbf{x}_n, \mathbf{w}) - \mathbf{t}_n\right\rVert^2 ,
\end{equation}

\noindent
where ${\mathbf{x}_n}$ (for $n = 1, ..., N$) is a training set comprising of $N$ input vectors, $\mathbf{y}(\mathbf{x}_n, \mathbf{w})$ is network's output vector for given input $\mathbf{x}_n$, $\mathbf{w}$ is weights vector and $\mathbf{t}_n$ is a target vector corresponding to $n$-th sample in training set.

The update of the parameters is done on a small sets of the training data, called \emph{batches}. Depending on the size of those batches, Gradient Descent algorithms can be further divided into Stochastic Gradient Descent (SGD), Batch Gradient Descent and Mini-batch Gradient Descent, where SGD uses one training sample at a time, batch methods use whole training set at a time and Mini-batch methods use between one and all samples. One complete cycle through all the training data is called an \emph{epoch} and can be repeated in order to increase the accuracy of the NN.

The number of epochs or the number of samples in one batch are an important hyper-parameters of the network, because suitable settings of these hyper-parameters can improve generalisation and prevent underfitting or overfitting of the NN. Underfitting problem is when the network is not able to make correct predictions on unseen data because it's too simple or isn't trained well enough. Overfitting occurs when the NN is too complex and over-adapted to the training data. That leads to loosing the ability of generalisation and therefore poor accuracy on the testing data. However in contrast to other hyper-parameters, a suitable number of epochs can be easily found using training and validation loss as described in Section \ref{ssec:learning_hyper-parameters}.

Apart from the mentioned Gradient Descent algorithms, there are many alternatives\footnote{\url{https://en.wikipedia.org/wiki/Outline_of_machine_learning\#Machine_learning_algorithms}}. The most used methods are also gradient-based, but others such as Simulated Annealing~\cite{sexton_bbusatnn} or Evolutionary Programming~\cite{fogel_eptnn} are derivative-free. These algorithms have their specific hyper-parameters, but their description is out of the scope of this thesis.

% ----------------------------------------------------------------------------- %
% ----------------------------------------------------------------------------- %
\section{Hyper-parameters of Neural Networks}
\label{sec:hyper-parameters}

As mentioned before, the hyper-parameters influence the structure or the learning process of the NN and need to be explicitly set before training the network. They have a significant influence on the accuracy of the NN, so it is beneficial to know how they influence the NN in order to optimise them. Even though not all NNs share the same structure or use the same learning algorithms, they frequently use the same common hyper-parameters that have a similar influence on the resulting behaviour of the NN.

Common hyper-parameters that define NN structure are number of hidden layers, number of neurons in the hidden layer, and activation function. These hyper-parameters are further described in Section~\ref{ssec:structural_hyper-parameters}. Learning of the NNs is influenced by hyper-parameters such as learning rate, dropout, momentum, number of epochs, batch size and weight decay, further described in Section~\ref{ssec:learning_hyper-parameters}. Though features described below have specific characteristics, their effect may differ in individual networks.

% ----------------------------------------------------------------------------- %
\subsection{Structure related Hyper-parameters}
\label{ssec:structural_hyper-parameters}

\textbf{The number of hidden layers} is shown in Figure~\ref{fig:ann_architecure} and it determines the complexity of a problem that is the NN able to solve. Networks with~\cite{garang_bampmhnhl}:

\begin{itemize}
  \item{\bf no hidden layers \rm -- can solve only linearly separable problems}
  \item{\bf one hidden layer \rm -- can solve almost any problem that contains a continuous mapping from one finite space to another}
  \item{\bf two hidden layers \rm -- can be used to model data with discontinuities such as saw tooth wave pattern}
  \item{\bf more than two layers \rm -- have no theoretical reason to be used but in practice can achieve better results}
\end{itemize}

\noindent
So while the higher number of layers can improve the accuracy of the NN, using too many hidden layers may lead to problems such as overfitting or vanishing gradient.

\textbf{The number of neurons in hidden layer} is a main measure in ability of NN to learn a particular function. Too few hidden neurons can lead to inability to learn the function (underfitting), too many hidden neurons can lead to overfitting and increase of time needed to train the NN~\cite{garang_bampmhnhl}. While it is possible to have different number of units in each hidden layer, many networks use the same number for every hidden layer. There are many rule-of-thumb methods, such as~\cite{garang_bampmhnhl}:

\begin{itemize}
  \item{The number of hidden neurons should be between the size of the input layer and the size of the output layer.}
  \item{The number of hidden neurons should be 2/3 the size of the input layer, plus the size of the output layer.}
  \item{The number of hidden neurons should be less than twice the size of the input layer.}
\end{itemize}

\noindent
These suggestions can help with the selection of the number of hidden neurons, but they are more of a starting point than a rule.

\textbf{Activation function} determines the output of each layer in the network and has a major influence on network's accuracy, convergence and computational efficiency.

The activation function can be linear or non-linear. The linear activation functions can be represented by a straight line and have unconfined output. Because of the fact that linear combination of multiple functions is still a linear function, all subsequent layers with linear activation functions collapse into one. That is why the modern NNs use non-linear activations functions that enable the creation of deep NNs. It is common to use different activation functions for hidden layers and output layer, depending on what behaviour is desired. The hidden layers widely use ReLU activation functions for their features, which are described below. The activation function in the output layer depends on the desired output (i.e. whether it is for regression, classification, clustering, ...).

Different activation functions are used for their features which may be better in solving different problems. Sigmoid activation function has smooth gradient and provides clear predictions. For example as can be seen in Figure~\ref{fig:af_sigmoid}, for $x$ values outside of an interval $[-2, 2]$, $y$ values are pretty close to $1$ or $0$. The output is confined on the interval between $0$ and $1$. Disadvantages of sigmoid function are that it's not zero centred, can lead to vanishing gradient problem (for very low/high $x$ values) and is computationally expensive. In practice, sigmoid activation function is commonly used in output layer in classification problems.

As can be seen in Figure~\ref{fig:af_tanh}, hyperbolic tangent function has quite similar qualities as the sigmoid function. But it's zero centred, which means the output is in range $[-1, 1]$, strongly negative values are mapped to values close to $-1$, values close to $0$ are mapped to values close to $0$ and strongly positive values are mapped to values close to $1$. That makes it easier to model inputs that have strongly negative, neutral and strongly positive values~\cite{lecun_eb}. Hyperbolic tangent function is commonly used in classification problems.

\begin{figure}[h]
  \begin{subfigure}{.45\textwidth}
    \centering
    \fbox{\includegraphics[width=.9\linewidth]{obrazky-figures/nn/sigmoid_activation_fn.pdf}}
    \caption{Sigmoid} % $\frac{1}{1 + e^{-x}}$
    \label{fig:af_sigmoid}
  \end{subfigure}
  \hfill
  \begin{subfigure}{.45\textwidth}
    \centering
    \fbox{\includegraphics[width=.9\linewidth]{obrazky-figures/nn/tanh_activation_fn.pdf}}
    \caption{Hyperbolic tangent}
    \label{fig:af_tanh}
  \end{subfigure}
  \hfill
  \begin{subfigure}{.45\textwidth}
    \centering
    \fbox{\includegraphics[width=.9\linewidth]{obrazky-figures/nn/relu_activation_fn.pdf}}
    \caption{ReLU}
    \label{fig:af_relu}
  \end{subfigure}
  \hfill
  \begin{subfigure}{.45\textwidth}
    \centering
    \fbox{\includegraphics[width=.9\linewidth]{obrazky-figures/nn/relu-like_activation_fns.pdf}}
    \caption{Other ReLU-like functions}
    \label{fig:af_relu_like}
  \end{subfigure}
  \caption{Common non-linear activation functions}
  \label{fig:activation_functions}
\end{figure}

Rectified Lineary Unit (ReLU) is a function where all negative values are mapped to zero and all positive values are mapped to identical values. ReLU is computationally efficient and the output is half-opened interval $[0, \infty)$. The problem of ReLU functions are zero and negative values, which are immediately mapped to zero (see Figure~\ref{fig:af_relu}). That decreases the ability of the model to fit or train from the data properly~\cite{lu_dritne}, which is known as the dying ReLU problem.

To prevent the dying ReLU problem there are multiple similar functions such as Leaky ReLU, Parametric ReLU \cite{prajit_saf} or ELU (Exponential Linear Unit) \cite{clevert_fadnlelu}, which help increase the output range by having a small non-zero slope for negative values. Examples of such activation functions are in Figure~\ref{fig:af_relu_like}.

% ----------------------------------------------------------------------------- %
\subsection{Learning related Hyper-parameters}
\label{ssec:learning_hyper-parameters}

\textbf{Learning rate} (LR) is used in gradient descent algorithm when parameters are updated according to an optimisation function~\cite{alto_tds}. Typically, values of the learning rate are a small positive numbers between $0$ and $1$. When optimising the learning rate, its values are usually sampled from log-space and suitable values are highly dependent on batch normalisation~\cite{bjorck_ubn} that enables training with larger learning rate. Too low learning rate converges to the minimum smoothly, but slows down the learning process (Figure~\ref{fig:lr_too_low}). Too high learning rate speeds up the learning process, but may not converge (Figure~\ref{fig:lr_too_high}).

\begin{figure}[h]
  \centering
  \begin{subfigure}[t]{.45\textwidth}
    \centering
    \includegraphics[width=.9\linewidth]{obrazky-figures/nn/gd_low_lr.pdf}
    \caption{Example of too low learning rate leading to slow convergence.}
    \label{fig:lr_too_low}
  \end{subfigure}
  \hfill
  \begin{subfigure}[t]{.45\textwidth}
    \centering
    \includegraphics[width=.9\linewidth]{obrazky-figures/nn/gd_high_lr.pdf}
    \caption{Too high learning rate may cause oscillation around local minimum or even lead to divergence.}
    \label{fig:lr_too_high}
  \end{subfigure}
  \caption{Problems of too low or too high learning rate. The red arrow represents single step in Gradient Descent algorithm, where each case used the same initial weight $w_0$.}
  \label{fig:lr_problems}
\end{figure}

\textbf{Dropout} is a regularization method that is used to prevent overfitting in NNs by randomly ignoring $p$ neurons during the training phase. It means that for each training sample and each hidden layer, random fraction of hidden neurons are not considered. Then in testing phase, activation functions of the entire NN are considered, but each activation function is reduced by a factor $p$ to account for the neurons ignored in training phase~\cite{srivastava_dswpnno}.

\textbf{Weight decay} is another regularization method that is used to prevent overfitting. It penalizes large weights by modifying the error function:

\begin{equation}
    \widetilde{E}(\mathbf{w}) = E(\mathbf{w}) + \frac{\lambda}{2} \left\lVert \mathbf{w}\right\rVert^2,
\end{equation}

\noindent
where $\left\lVert \mathbf{w}\right\rVert^2 \equiv \mathbf{w}^T \mathbf{w} = w_0^2 + w_1^2 + \dotsc + w_M^2$, and the coefficient $\lambda$ governs the relative importance of the regularization term compared with the error term in error function $E(\cdot)$~\cite{bishop_prml}. Usual settings of coefficient $\lambda$ range between logarithmic values of $0$ and $0.1$. When weight decay is too high, the model may never fit quite well. When weight decay is too low, it might not prevent overfitting~\cite{vasani_ttcwd}.

\textbf{Momentum} controls how much the previous weight update influences the current weight update. This can speed up the learning process by making more significant update of weights when minimum is in the opposite direction of gradient. This might be especially helpful when optimisation reaches \emph{plateau}, an area where the error function decreases very slowly and thus gradient is small. Also, momentum can help overcome local minimum as shown in Figure~\ref{fig:gd_momentum}. Momentum is a number between $0$ and $1$ and it is common to use values close to $1$ ($0.9$, $0.99$, etc.)~\cite{goodfellow_dl}. Too small values have negligible effect and too big values are more likely to miss the optimum and lead to longer learning time.

\begin{figure}[h]
    \centering
    \includegraphics[width=.7\linewidth]{obrazky-figures/nn/gd_momentum.pdf}
    \caption{Example of how momentum can help Gradient Descent to get out of local minimum. Optimisation begins in point $1$ with weight $w_0$. In this point momentum is $0$ (since it is the first step) and the next weight update depends solely on the gradient and the learning rate. In the next steps, weight update is given by gradient and momentum addition. If momentum was lower that gradient in step $3$, weight would shift back to the local minimum. But if momentum is significant enough, it can help get to other minima (though getting out of local minimum or getting to global minimum is not guaranteed).}
    \label{fig:gd_momentum}
\end{figure}

\textbf{Number of epochs} is a number of complete cycles of learning algorithm through the whole dataset, which means it determines number of times the weights are updated. Too few epochs may cause underfitting, too many may lead to overfitting. Suitable number of epochs is usually found using training and validation loss, as shown in Figure~\ref{fig:number_of_epochs}. The goal is to find the highest possible number of epochs, before validation loss starts to grow because of overfitting.

\begin{figure}[h]
    \centering
    \includegraphics[width=.7\linewidth, keepaspectratio]{obrazky-figures/nn/number_of_epochs.pdf}
    \caption{An illustrative example showing the influence of the number of epochs on the training and validation error (Adapted from Wikimedia Commons). Too many epochs can lead to overfitting, but it's possible to set suitable number of epochs based on the training and testing error. Suitable number of epochs is the highest possible number before the testing error starts to grow.}
    \label{fig:number_of_epochs}
\end{figure}

\textbf{Batch size} defines number of samples used at once while training the NN. The range of batch size is from $1$ up to a size of the training set. Smaller batch size causes more frequent model updates and allows more robust convergence that can lead to better accuracy, but can also lead to less accurate estimate of gradient (especially in more complex datasets)~\cite{goodfellow_dl}.
