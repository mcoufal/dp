#!/usr/bin/env python3

import numpy as np
from matplotlib import pyplot as plt
import random


# grid search + random search points example
img_size = 32
nb_samples = 5
x_range = ([0, 5])
y_range = ([0, 3])
x = np.linspace(x_range[0], x_range[1], nb_samples)
y = np.linspace(y_range[0], y_range[1], nb_samples)

# font size
plt.rcParams.update({'font.size': 32})

# draw random coordinates
fig = plt.figure(figsize=(img_size, img_size))
plt.xlabel('x_1')
plt.ylabel('x_2')
ax = fig.add_subplot(111)
ax.grid(color='0.75', linestyle='--', linewidth=1)
for i in range(25):
    x_rand = random.uniform(x_range[0], x_range[1])
    y_rand = random.uniform(y_range[0], y_range[1])
    plt.scatter(x_rand, y_rand, marker="s", s=100, c="r", zorder=10)
plt.axis('scaled')
ax.set_xlim([0,5])
ax.set_ylim([0,3])
plt.show()
fig.savefig("random_search_space_2D.pdf", bbox_inches='tight')

# draw grid coordinates
fig = plt.figure(figsize=(img_size, img_size))
plt.xlabel('x_1')
plt.ylabel('x_2')
ax = fig.add_subplot(111)
ax.grid(color='0.75', linestyle='--', linewidth=1)
x_grid, y_grid = np.meshgrid(x, y)
plt.scatter(x_grid, y_grid, marker="s", s=100, c="b", zorder=10)
plt.axis('scaled')
ax.set_xlim([-0.2,5.2])
ax.set_ylim([-0.2,3.2])
plt.show()
fig.savefig("grid_search_space_2D.pdf", bbox_inches='tight')

# draw both in the same plot
fig = plt.figure(figsize=(img_size, img_size))
plt.xlabel('x_1')
plt.ylabel('x_2')
ax = fig.add_subplot(111)
ax.grid(color='0.75', linestyle='--', linewidth=1)
# random
for i in range(25):
    x_rand = random.uniform(x_range[0], x_range[1])
    y_rand = random.uniform(y_range[0], y_range[1])
    plt.plot(x_rand, y_rand, "s", c="r")
# grid
x_grid, y_grid = np.meshgrid(x, y)
plt.plot(x_grid, y_grid, "s", c="b")

plt.axis('scaled')
ax.set_xlim([-0.2,5.2])
ax.set_ylim([-0.2,3.2])
plt.show()
fig.savefig("grid_random_search_space_2D.pdf", bbox_inches='tight')
