#!/usr/bin/env python3

import numpy as np
from matplotlib import pyplot as plt
import random


def test_fn(x):
    return (x - 2)**2 + 1

x = np.linspace(0, 5, 50)
x_base = np.linspace(-1, 6, 50)
x_grid = np.linspace(0, 5, 5)
y_grid = test_fn(x_grid)

plt.rcParams.update({'font.size': 32})
img_size = 32

# GRID
fig = plt.figure(figsize=(img_size, img_size))
plt.xlabel('x_1')
plt.ylabel('y')
ax = fig.add_subplot(111)
ax.set_facecolor('w')
ax.grid(color='0.75', linestyle='--', linewidth=1)
ax.set_xlim([-0.2,5.2])
ax.set_ylim([0,12])
# baseline
ax.axhline(y=0, color='k', linestyle='-')
plt.plot(x_base, test_fn(x_base), c="k")
# stem
plt.stem(x_grid, y_grid, linefmt='k-.', markerfmt=None, basefmt=' ')
plt.stem([x_grid[2]], [y_grid[2]], linefmt='b-.', markerfmt=None, basefmt=' ')
# draw grid
plt.plot(x_grid, y_grid, "o", c="b")
plt.scatter(x_grid, np.zeros(5) + 0.1, marker="s", s=100, c="b", zorder=10)
print("Best GRID: [{}]: {}".format(x_grid[2], y_grid[2]))

plt.show()
fig.savefig("grid_search_space_1D.pdf", bbox_inches='tight')

# RANDOM
fig = plt.figure(figsize=(img_size, img_size))
ax = fig.add_subplot(111)
plt.xlabel('x_1')
plt.ylabel('y')
ax.set_facecolor('w')
ax.grid(color='0.75', linestyle='--', linewidth=1)
ax.set_xlim([-0.2,5.2])
ax.set_ylim([0,12])
# baseline
ax.axhline(y=0, color='k', linestyle='-')
plt.plot(x_base, test_fn(x_base), c="k")
best_x = -1
best_y = 999
for i in range(5):
    x_rand = random.uniform(0, 5)
    y = test_fn(x_rand)
    if y < best_y:
        best_y = y
        best_x = x_rand
    plt.stem([x_rand], [y], linefmt='k-.', markerfmt='r', basefmt=' ')
    plt.plot(x_rand, y, "o", c="r")
    plt.scatter(x_rand, 0.1, marker="s", s=100, c="r", zorder=10)

plt.stem([best_x], [best_y], linefmt='r-.', markerfmt='r', basefmt=' ')
print("Best RANDOM: [{}]: {}".format(best_x, best_y))

plt.show()
fig.savefig("random_search_space_1D.pdf", bbox_inches='tight')
