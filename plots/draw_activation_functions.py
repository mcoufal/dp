#!/usr/bin/env python3

import numpy as np
from matplotlib import pyplot as plt
import random

# SIGMOID
def sigmoid(x):
    return 1 / (1 + np.e**(-x))

# TANH
def tanh(x):
    return np.tanh(x)

# RELU
def relu(x):
    return np.maximum(0,x)

# ELU
def elu(x, a=1.0):
    N = len(x)
    res = np.zeros(N)
    for i in range(N):
        if x[i] < 0:
            res[i] = a * (np.exp(x[i]) - 1)
        else:
            res[i] = x[i]
    return res

# LEAKY RELU
def lrelu(x):
    N = len(x)
    res = np.zeros(N)
    for i in range(N):
        if x[i] < 0:
            res[i] = 0.01 * x[i]
        else:
            res[i] =  x[i]
    return res

# PARAMETRIC RELU
def prelu(x, a=0.1):
    N = len(x)
    res = np.zeros(N)
    for i in range(N):
        if x[i] < 0:
            res[i] = a * x[i]
        else:
            res[i] =  x[i]
    return res

# SOFTMAX
def softmax(x):
    e_x = np.exp(x - np.max(x))
    return e_x / e_x.sum(axis=0)

def center_axis(ax):
    # set the x-spine (see below for more info on `set_position`)
    ax.spines['left'].set_position('zero')
    # turn off the right spine/ticks
    ax.spines['right'].set_color('none')
    ax.yaxis.tick_left()
    # set the y-spine
    ax.spines['bottom'].set_position('zero')
    # turn off the top spine/ticks
    ax.spines['top'].set_color('none')
    ax.xaxis.tick_bottom()


# SIZES
plt.rcParams.update({'font.size': 32})
img_size = 32
lw = 4 # line width

# DRAW SIGMOID ACTIVATION FUNCTION
x = np.linspace(-5, 5, 500)
fig = plt.figure(figsize=(img_size, img_size))
#plt.xlabel('x')
#plt.ylabel('y')
ax = fig.add_subplot(111)
ax.set_facecolor('w')
ax.grid(color='0.75', linestyle='--', linewidth=1)
center_axis(ax)
# draw activation function
plt.plot(x, sigmoid(x), label="Sigmoid", linewidth=lw)
#ax.legend()
plt.show()
fig.savefig("sigmoid_activation_fn.pdf", bbox_inches='tight')

# DRAW TANH ACTIVATION FUNCTION
x = np.linspace(-5, 5, 500)
fig = plt.figure(figsize=(img_size, img_size))
#plt.xlabel('x')
#plt.ylabel('y')
ax = fig.add_subplot(111)
ax.set_facecolor('w')
ax.grid(color='0.75', linestyle='--', linewidth=1)
center_axis(ax)
# draw activation function
plt.plot(x, tanh(x), label="TanH", linewidth=lw)
#ax.legend()
plt.show()
fig.savefig("tanh_activation_fn.pdf", bbox_inches='tight')

# DRAW RELU ACTIVATION FUNCTION
x = np.linspace(-5, 5, 500)
fig = plt.figure(figsize=(img_size, img_size))
#plt.xlabel('x')
#plt.ylabel('y')
ax = fig.add_subplot(111)
ax.set_facecolor('w')
ax.grid(color='0.75', linestyle='--', linewidth=1)
center_axis(ax)
# draw activation function
plt.plot(x, relu(x), label="ReLU", linewidth=lw)
#ax.legend()
plt.show()
fig.savefig("relu_activation_fn.pdf", bbox_inches='tight')

# DRAW RELU-LIKE ACTIVATION FUNCTIONS
x = np.linspace(-5, 5, 500)
fig = plt.figure(figsize=(img_size, img_size))
#plt.xlabel('x')
#plt.ylabel('y')
ax = fig.add_subplot(111)
ax.set_facecolor('w')
ax.grid(color='0.75', linestyle='--', linewidth=1)
center_axis(ax)
# draw activation function
plt.plot(x, elu(x), label="ELU", linewidth=lw)
plt.plot(x, lrelu(x), label="Leaky ReLU", linewidth=lw)
plt.plot(x, prelu(x), label="PReLU", linewidth=lw)
ax.legend()
plt.show()
fig.savefig("relu-like_activation_fns.pdf", bbox_inches='tight')

# DRAW SOFTMAX ACTIVATION FUNCTION
x = np.linspace(-5, 5, 500)
fig = plt.figure(figsize=(img_size, img_size))
#plt.xlabel('x')
#plt.ylabel('y')
ax = fig.add_subplot(111)
ax.set_facecolor('w')
ax.grid(color='0.75', linestyle='--', linewidth=1)
center_axis(ax)
# draw activation function
plt.plot(x, softmax(x), label="Softmax")
#ax.legend()
plt.show()
fig.savefig("softmax_activation_fn.pdf", bbox_inches='tight')
