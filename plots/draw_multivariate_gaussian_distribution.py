#!/usr/bin/env python3

import numpy as np
from numpy import ma
import matplotlib.pyplot as plt
from matplotlib import cm
from scipy.stats import multivariate_normal
from mpl_toolkits.mplot3d import Axes3D

plt.rcParams.update({'font.size': 22})

# parameters to set
mu_x = 0
variance_x = 3.74
mu_y = 0
variance_y = 3.74
sigma_xy = 2.74

# create grid and multivariate normal distribution
x = np.linspace(-10,10,500)
y = np.linspace(-10,10,500)
X, Y = np.meshgrid(x,y)
pos = np.empty(X.shape + (2,))
pos[:, :, 0] = X; pos[:, :, 1] = Y
rv = multivariate_normal([mu_x, mu_y], [[variance_x, sigma_xy], [sigma_xy, variance_y]])

# 3D PLOT
cmap1 = cm.coolwarm
fig = plt.figure()
ax = fig.gca(projection='3d')
ax.set_facecolor('w')
ax.plot_surface(X, Y, rv.pdf(pos),cmap=cmap1,linewidth=0)
ax.set_xlabel('X axis')
ax.set_ylabel('Y axis')
ax.set_zlabel('Z axis')
plt.show()
fig.savefig("mgd_3D.pdf", bbox_inches='tight')


# CONTOUR PLOT 1
cmap2 = cm.coolwarm
cmap2.set_under(color='white')
pos = np.array([X.flatten(),Y.flatten()]).T
rv = multivariate_normal([mu_x, mu_y], [[variance_x, sigma_xy], [sigma_xy, variance_y]])
fig = plt.figure(figsize=(10,10))
plt.xlabel('x1')
plt.ylabel('x2')
ax0 = fig.add_subplot(111)
ax0.contour(rv.pdf(pos).reshape(500,500), cmap=cmap2, linewidths=5)
plt.show()
fig.savefig("mgd_contour1.pdf", bbox_inches='tight')

# CONTOUR PLOT 2
variance_x = 3.74
variance_y = 3.74
sigma_xy = -2.74

cmap2 = cm.coolwarm
cmap2.set_under(color='white')
pos = np.array([X.flatten(),Y.flatten()]).T
rv = multivariate_normal([mu_x, mu_y], [[variance_x, sigma_xy], [sigma_xy, variance_y]])
fig = plt.figure(figsize=(10,10))
plt.xlabel('x1')
plt.ylabel('x2')
ax0 = fig.add_subplot(111)
ax0.contour(rv.pdf(pos).reshape(500,500), cmap=cmap2, linewidths=5)
plt.show()
fig.savefig("mgd_contour2.pdf", bbox_inches='tight')
